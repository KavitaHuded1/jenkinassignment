package com.wipro.scientificalculator;

public class TrigCalc {
	public double getSinValue(int degree) {
        return Math.sin(Math.toRadians(degree));
    }
}
