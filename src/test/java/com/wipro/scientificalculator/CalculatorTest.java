package com.wipro.scientificalculator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.wipro.scientificalculator.TrigCalc;


public class CalculatorTest {
	
	    @Test
    public void testGetSinValue() {
        TrigCalc calculator = new TrigCalc();
        Assertions.assertEquals(0.0, calculator.getSinValue(0));
        // Adjusted assertion for degree = 30
        Assertions.assertTrue(Math.abs(0.5 - calculator.getSinValue(30)) < 0.0001);
        // Corrected test case for degree = 45
        Assertions.assertTrue(Math.abs(Math.sin(Math.toRadians(45)) - calculator.getSinValue(45)) < 0.0001);
    }
}
